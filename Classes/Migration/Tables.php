<?php

    namespace ChefDeploy\Migration;

    use Cuisine\Wrappers\Schema;

    class Tables{

        /**
         * Table prefix
         *
         * @var String
         */
        protected $prefix;

        /**
         * Migration table prefix
         *
         * @var String
         */
        protected $migPrefix;

        /**
         * Constructor
         */
        public function __construct()
        {
            global $table_prefix;
            $this->prefix = $table_prefix;
            $this->migPrefix = '_mig_'.$this->prefix;
        }

        /**
         * Cleanup tables
         *
         * @return void
         */
        public function cleanUp()
        {
            $tables = $this->get();
            
            foreach( $tables as $table ){

                if( $this->exists( $this->migPrefix . $table ) ){

                    //delete the original:
                    $this->drop( $this->prefix . $table );

                    //rename the migration table:
                    $this->rename( $table );
                }
            }
        }

        /**
         * Rename a table
         * 
         * @param String $table
         * @return void
         */
        public function rename( $table )
        {
            global $wpdb;
            $from = $this->migPrefix . $table;
            $to = $this->prefix . $table;   
        
            $sql = "RENAME TABLE $from TO $to";
			$wpdb->query( $sql );
        }

        /**
         * Drop a table
         *
         * @param String $table
         * @return void
         */
        public function drop( $table )
        {
            global $wpdb;
            $sql = "DROP TABLE $table";
            $wpdb->query( $sql );
        }

        /**
         * Check if the table in question exists:
         *
         * @param String $tableName
         * @return Bool
         */
        public function exists( $tableName )
        {
            global $wpdb;
            return ( $wpdb->get_var( "SHOW TABLES LIKE '$tableName'" ) == $tableName );
        }

        /**
         * Returns sanitized array of table names
         *
         * @return Array
         */
        public function get()
        {
            global $wpdb;
            $results = $wpdb->get_results( "SHOW TABLES LIKE '$this->prefix%'", ARRAY_N );
            $tables = [];

            foreach( $results as $result ){
                $tables[] = str_replace( $this->prefix, '', $result[0] );
            }

            return $tables;
        }
        

    }