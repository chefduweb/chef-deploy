<?php

	namespace ChefDeploy\Migration;

	use WP_Roles;
	use ChefDeploy\Helpers\UserRoles;
	use ChefDeploy\Wrappers\Environment;	
	use ChefDeploy\Wrappers\StaticInstance;
	use ChefDeploy\Migration\Requests\UserRolesRequest;
	use ChefDeploy\Migration\Requests\FetchPrefixRequest;
	use ChefDeploy\Migration\Handlers\UserRolesHandler;
    use ChefDeploy\Migration\Requests\RemotePauseRequest;

	class EventListener extends StaticInstance{

		/**
		 * Constructor
		 */
		public function __construct()
		{
			$this->listen();
            $this->routes();
            
		}

		/**
		 * Listen for table alterations, and hook them
		 * 
		 * @return void
		 */
		public function listen()
		{

            /*add_action( 'init', function(){
                $env = Environment::staging();
                dd( MigrationHandler::getRemotePrefix( $env ) );
            });*/

			//prefix local
			add_filter( 'wpmdb_target_table_name', function( $tableName, $action, $stage, $siteDetails = [] ){
				
				return MigrationHandler::localPrefix( $tableName, $action, $stage, $siteDetails);

			}, 4, 20 );

			

			//prefix remote
			add_filter( 'wpmdb_finalize_target_table_name', function( $tableName, $intent, $siteDetails = [] ){

				return MigrationHandler::remotePrefix( $tableName, $intent, $siteDetails );

			}, 3, 20 );


			//media filters:
			add_filter( 'wpmdbmf_destination_file_path', function( $path, $type, $remote ){

				return MigrationHandler::pushMedia( $path );

			}, 100, 3 );

			
			add_filter( 'chef_upload_media', function( $path ){
				
				return MigrationHandler::pullMedia( $path );
				
			});
			

			/**
			 * Only set wpdb tables on a multisite-fetch
			 */
			add_filter('wpmdb_tables', function( $clean_tables, $scope ){

				global $wpdb;

				if( is_multisite() ){

					$settings 	  = get_site_option( 'current_deployment' );
                    $prefix       = ( $scope == 'temp' ? '_mig_' : $settings['prefix'] );

					$tables       = $wpdb->get_results( 'SHOW FULL TABLES', ARRAY_N );
					$clean_tables = array();

					foreach ( $tables as $table ) {

						if ( ( ( $scope == 'temp' || $scope == 'prefix' ) && 0 !== strpos( $table[0], $prefix ) ) || $table[1] == 'VIEW' ) {
							continue;
						}

						$clean_tables[] = $table[0];
					}

				}

				return $clean_tables;
				
			}, 100, 2 );

			//preserve wpdb migrate pro settings:
			add_filter( 'wpmdb_preserved_options', function( $options ){

				$options[] = 'wpmdb_settings';
				return $options;
			});


			//reset roles if we have none:
			add_action( 'after_setup_theme', function(){
				$roles = new WP_Roles();
				if( 
					!is_array( $roles->roles ) || 
					empty( $roles->roles ) ||
					get_site_option( 'user_roles', false ) == false
				){
					UserRoles::reset();

					//reset the fucking global because, ugh; WordPress...
					unset( $GLOBALS['wp_roles'] );
					unset( $GLOBALS['wp_user_roles'] );
					$GLOBALS['wp_roles'] = new WP_Roles();
				}

			});
			
			//remove admin menu:
			//add_filter( 'admin_menu', function(){
			//	remove_submenu_page( 'tools.php', 'wp-migrate-db-pro' );
            //});
            
            add_action( 'admin_init', function(){

                if( isset( $_GET['stopContentPause'] ) )
                    update_option( 'site_content_paused', false );

                $paused = get_option( 'site_content_paused', false );
                if( $paused && is_multisite() ){
                    wp_die( 'Content-editting for this site is paused.' );
                }else if( $paused ){
                    delete_option( 'site_content_paused' );
                }

            });
		}


		/**
		 * Sets up routes
		 * 
		 * @return String
		 */
		public function routes()
		{
			
			add_action( 'pre_get_posts', function(){

				global $wp;
				$requestName = null;

				if( isset( $wp->query_vars['name'] ) )
					$requestName = $wp->query_vars['name'];

				if( isset( $wp->request ) && is_null( $requestName ) )
					$requestName = $wp->request;


				if( !is_null( $requestName ) ){

					if( 
                        $requestName == 'fetchPrefix' || 
                        $requestName == 'remoteOptions' ||
                        $requestName == 'remotePause' ||
                        $requestName == 'blogId' ||
                        $requestName == 'addAdmins'
                    ){

						$name = ucwords( $requestName );
						$nsp = '\\ChefDeploy\\Migration\\';
						$rClass = $nsp.'Requests\\'.$name.'Request';
						$hClass = $nsp.'Handlers\\'.$name.'Handler';

						$request = new $rClass();

						if( $request->valid() )
							( new $hClass( $request ) )->process();


						die();
					}
				}
			});	
		

			/**
			 * Test deploys:
			 */
			add_action( 'init', function(){

				if( isset( $_GET['testDeploy'] ) ){

					$env = Environment::staging();
					$req = RemotePauseRequest::create( $env, true );

					//wp_redirect( $req );
					dd( $req );

				}
			});
		}

	}


	\ChefDeploy\Migration\EventListener::getInstance();