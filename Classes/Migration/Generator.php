<?php

	namespace ChefDeploy\Migrations;

	use ChefDeploy\Wrappers\StaticInstance;

	class Generator extends StaticInstance{


		/**
		 * Constructor
		 */
		public function __construct()
		{

			if( !get_option( 'profiles-generated', false ) || isset( $_GET['regenerate-profiles'] ) )
				$this->generate();

		}


		/**
		 * Generate the different WP DB Migrate Pro profiles
		 * 
		 * @return void
		 */
		public function generate()
		{

			$data = [];
			foreach( $this->getProfiles() as $profile ){

				$class = $this->getClassName( $profile );
				$profileToGenerate = new $class();
				
				if( $profileToGenerate->generate() )
					$data[] = $profile;
		
			}

			if( !empty( $data ) )
				update_option( 'profiles-generated', $data );
		}


		/**
		 * Creates the class name out of a profile-string
		 *
		 * @param String $profile
		 * 
		 * @return String
		 */
		public function getClassName( $profile )
		{
			//create class-name
			$class = ucwords( $profile );
			$class = str_replace( '->', 'To', $class );
			$class = str_replace( ' ', '', $class );
			return '\\ChefDeploy\\Migration\\Profiles\\'.$class;
		}


		/**
		 * Return the profiles
		 * 
		 * @return Array
		 */
		public function getProfiles()
		{
			$profiles = [
				'single -> staging',
				'single -> production',
				'staging -> single',
				'production -> single'
			];

			$profiles = apply_filters( 'chef-multisite-profiles', $profiles );
			return $profiles;
		}


	}

	\ChefDeploy\Migrations\Generator::getInstance();