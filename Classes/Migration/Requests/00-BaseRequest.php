<?php

	namespace ChefDeploy\Migration\Requests;
	

	use Exception;
	use Cuisine\Utilities\Fluent;

	abstract class BaseRequest{


		/**
		 * Properties
		 * 
		 * @var Fluent
		 */
		protected $props;


		/**
		 * Array with possible errors
		 * 
		 * @var Array
		 */
		protected $errors;

		/**
		 * Constructor
		 * 
		 */
		public function __construct()
		{
			$this->props = new Fluent();
		}



		/**
		 * Check to see if this request is valid
		 * 
		 * @return boolean
		 */
		public function valid()
		{
			return true;
		}


		/**
		 * Get request properties
		 * 
		 * @return Mixed
		 */
		public function get( $string )
		{
			return $this->sanitize( $string, $this->props->get( $string ) );
		}


		/**
		 * Sanitize the key-value pair
		 * 
		 * @param  String $key  
		 * @param  Mixed $value
		 * 
		 * @return Mixed
		 */
		public function sanitize( $key, $value )
		{
			return $value;
		}


		/**
		 * Add an error message
		 * 
		 * @param String $message
		 *
		 * @return void
		 */
		public function addError( $message )
		{
			$this->errors[] = $message;
		}


		/**
		 * Checks wether or not this request has errors
		 * 
		 * @return boolean
		 */
		public function hasErrors()
		{
			return ( empty( $this->errors ) ? false : true );
		}


		/**
		 * Returns the errors as a string
		 * 
		 * @return String
		 */
		public function getErrors()
		{
			if( !empty( $errors ) ) 
				return implode( "\n\r", $this->errors );

			return 'no errors';
		}
	}