<?php

	namespace ChefDeploy\Migration\Requests;

	use Cuisine\Utilities\Fluent;

	class BlogIdRequest extends BaseRequest{

		/**
		 * Constructor
		 */
		public function __construct()
		{
			$this->props = new Fluent( ['sitepath' => $_GET['sitepath'] ] );
        }
        

	}