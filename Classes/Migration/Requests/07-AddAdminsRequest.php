<?php

	namespace ChefDeploy\Migration\Requests;

	use Cuisine\Utilities\Fluent;

	class AddAdminsRequest extends BaseRequest{

		/**
		 * Constructor
		 */
		public function __construct()
		{
			$this->props = new Fluent( ['sitepath' => $_GET['sitepath'] ] );
        }
        

	}