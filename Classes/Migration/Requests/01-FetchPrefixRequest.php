<?php

	namespace ChefDeploy\Migration\Requests;

	use Cuisine\Utilities\Fluent;

	class FetchPrefixRequest extends BaseRequest{



		/**
		 * Constructor
		 */
		public function __construct()
		{
			$this->props = new Fluent( $this->decipher() );
		}


		/**
		 * Create a new deploy request
		 *
		 * @param Environment $env
		 * 
		 * @return String;
		 */
		public static function create( $env, $pause = true )
		{

			global $wpdb;
			
			//only logged in users can do this:
			$req = base64_encode( 
				$env->get('wpuser').';'
				.time().';'
				.md5( time().static::getRequestName() ).';'
				.$env->get('url').';'
				.$env->get('sitepath').';'
				.$wpdb->base_prefix.';'
			);


			$baseUrl = trailingslashit( $env->get('url') ) . static::getRequestName();
			$url = add_query_arg( 'req', $req, $baseUrl ); //link to the environment
			return $url;

		}



		/**
		 * Returns the name of a deploy request
		 * 
		 * @return String
		 */
		public static function getRequestName()
		{
			return 'fetchPrefix';
		}

		

		/**
		 * Sanitize the key-value pair
		 * 
		 * @param  String $key  
		 * @param  Mixed $value
		 * 
		 * @return Mixed
		 */
		public function sanitize( $key, $value )
		{
			switch( $key ){

				case 'sitepath':
					$path = $value;
					if( substr( $path, 0, 1 ) !== '/' )
						$path = '/'.$path;

					return trailingslashit( $path );
					break;

				case 'url':
					return str_replace(['https://', 'http://'], '', $this->get( 'site' ) );
					break;

				case 'name':
					return str_replace('/', '', $this->get( 'sitepath' ) );
					break;

			}

			return $value;
		}


		/**
		 * Returns wether or not this request is valid or not
		 * 
		 * @return Bool
		 */
		public function valid()
		{
			if( !isset( $_GET['req'] ) ){

				$this->addError( __( 'No valid request', 'chefdeploy' ) );
				return false;
			}

			if( is_null( $this->props->get( 'timestamp' ) ) ){

				$this->addError( __( 'No timestamp given', 'chefdeploy' ) );
				return false;
			}

			if( is_null( $this->props->get( 'nonce' ) ) ){

				$this->addError( __( 'No nonce given', 'chefdeploy' ) );
				return false;
			}

			if( is_null( $this->props->get( 'site' ) ) ){

				$this->addError( __( 'No site url given', 'chefdeploy' ) );
				return false;
			}


			if( !is_numeric( $this->props->get( 'timestamp' ) ) ){

				$this->addError( __( 'No valid timestamp', 'chefdeploy' ) );
				return false;
			}

			$newTimestamp = $this->props->get( 'timestamp' ) + ( 60 * 60 * 24 );
			if( $newTimestamp < time() ){

				$this->addError( __( 'Expired request', 'chefdeploy' ) );
				return false;
			}

			if( md5( $this->props->get( 'timestamp' ) .static::getRequestName() ) !== $this->props->get( 'nonce' ) ){

				$this->addError( __( 'Invalid nonce', 'chefdeploy' ) );
				return false;
			}


			if( !$this->userExists( $this->props->get( 'user' ) ) ){

				$this->addError( sprintf( __( 'User %s isn\'t familiar on the remote site' , 'chefdeploy' ), $this->props->get( 'user' ) ) );
				return false;
			}

			if( is_multisite() && is_null( $this->props->get('sitepath') ) ){

				$this->addError( __( 'This is a multisite and no path has been given', 'chefdeploy' ) );
				return false;
			}

			if( is_null( $this->props->get( 'prefix' ) ) ){

				$this->addError( __( 'No original database prefix given', 'chefdeploy' ) );
				return false;

			}

			return true;
		}


		/**
		 * Check if a user exists by checking the e-mail
		 * 
		 * @return bool
		 */
		public function userExists( $user ){

			return get_user_by( 'login', $user );

		}

		
		/**
		 * Returns a sanitized array
		 * 
		 * @return Array
		 */
		public function decipher()
		{
			if( isset( $_GET['req'] ) ){

				$encodedReq = $_GET['req'];
				$reqVars = base64_decode( $encodedReq );
				$reqVarsArr = explode( ";", $reqVars );
	
				return array(
					'user'			=> $reqVarsArr[0],
					'timestamp'		=> ( isset( $reqVarsArr[1] ) ? $reqVarsArr[1] : null ),
					'nonce'			=> ( isset( $reqVarsArr[2] ) ? $reqVarsArr[2] : null ),
					'site'			=> ( isset( $reqVarsArr[3] ) ? $reqVarsArr[3] : null ),
					'sitepath'		=> ( isset( $reqVarsArr[4] ) ? $reqVarsArr[4] : null ),
					'prefix'		=> ( isset( $reqVarsArr[5] ) ? $reqVarsArr[5] : null )
				);

			}

			return null;
		}


	}