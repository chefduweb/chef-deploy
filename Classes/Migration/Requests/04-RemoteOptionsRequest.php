<?php

	namespace ChefDeploy\Migration\Requests;

	use Cuisine\Utilities\Fluent;

	class RemoteOptionsRequest extends FetchPrefixRequest{




		/**
		 * Create a new deploy request
		 *
		 * @param Environment $env
		 * 
		 * @return String;
		 */
		public static function create( $env, $unpause = false )
		{

            global $wpdb;
            
            $unPauseString = ( $unpause ? 'unpause' : 'none' );

			//only logged in users can do this:
			$req = base64_encode( 
				$env->get('wpuser').';'
				.time().';'
				.md5( time().static::getRequestName() ).';'
				.$env->get('url').';'
				.$env->get('sitepath').';'
                .$wpdb->base_prefix.';'
                .$unPauseString.';'
			);


			$baseUrl = trailingslashit( $env->get('url') ) . static::getRequestName();
			$url = add_query_arg( 'req', $req, $baseUrl ); //link to the environment
			return $url;

		}

		/**
		 * Returns the name of a deploy request
		 * 
		 * @return String
		 */
		public static function getRequestName()
		{
			return 'remoteOptions';
		}	

        
	    /**
		 * Returns a sanitized array
		 * 
		 * @return Array
		 */
		public function decipher()
		{
			if( isset( $_GET['req'] ) ){

				$encodedReq = $_GET['req'];
				$reqVars = base64_decode( $encodedReq );
				$reqVarsArr = explode( ";", $reqVars );
	
				return array(
					'user'			=> $reqVarsArr[0],
					'timestamp'		=> ( isset( $reqVarsArr[1] ) ? $reqVarsArr[1] : null ),
					'nonce'			=> ( isset( $reqVarsArr[2] ) ? $reqVarsArr[2] : null ),
					'site'			=> ( isset( $reqVarsArr[3] ) ? $reqVarsArr[3] : null ),
					'sitepath'		=> ( isset( $reqVarsArr[4] ) ? $reqVarsArr[4] : null ),
                    'prefix'		=> ( isset( $reqVarsArr[5] ) ? $reqVarsArr[5] : null ),
                    'unpause'       => ( isset( $reqVarsArr[6] ) ? $reqVarsArr[6] : 'none' )
				);

			}

			return null;
		}
	}