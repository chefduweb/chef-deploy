<?php

	namespace ChefDeploy\Migration\Requests;

	use ChefDeploy\Helpers\Table;
	use ChefDeploy\Wrappers\Environment;

	class LocalPrefixRequest extends BaseRequest{


		/**
		 * Instance name
		 * 
		 * @var string
		 */
		protected $name = 'localPrefix';


		/**
		 * Table name
		 * 
		 * @var String
		 */
		public $tableName;


		/**
		 * Stage name
		 * 
		 * @var String
		 */
		protected $stage;


		/**
		 * Array with site details
		 * 
		 * @var Array
		 */
		public $details;


		/**
		 * Environment
		 * 
		 * @var ChefDeploy\Environments\Environment;
		 */
		public $env;


		/**
		 * Constructor
		 * 
		 * @param String $table_name   
		 * @param String $action       
		 * @param String $stage        
		 * @param array  $site_details 
		 *
		 * @return void
		 */
		public function __construct( $tableName, $stage, $siteDetails = array() )
		{
			$this->tableName = $tableName;
			$this->stage = $stage;
			$this->details = $siteDetails;

			//set the remote environment
			$remoteUrl = $this->getRemoteUrl();
			$this->env = Environment::getByUrl( $remoteUrl );
		}
		

		/**
		 * Checks wether this is a valid migration request for the given table
		 * 
		 * @return boolean
		 */
		public function valid()
		{
			
			//if it's a global table, return the regular table name
			if( Table::isGlobal( $this->tableName ) )
				return false;

			if( $this->stage == 'backup' )
				return false;

			if( is_null( $this->env ) ){
				throw new Exception( 'No valid environment found with url '.$this->getRemoteUrl() );
				return false;
			}

			return true;

		}


		/**
		 * Returns the remote url
		 * 
		 * @return String
		 */
		public function getRemoteUrl()
		{
			if( 
				isset( $this->details['remote']['site_url'] ) && 
				$this->details['remote']['site_url'] != '' 
			)
				return $this->details['remote']['site_url'];

			return '';

		}
	}