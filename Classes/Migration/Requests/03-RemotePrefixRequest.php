<?php

	namespace ChefDeploy\Migration\Requests;

	use ChefDeploy\Wrappers\Environment;

	class RemotePrefixRequest extends BaseRequest{


		/**
		 * Instance name
		 * 
		 * @var string
		 */
		protected $name = 'remotePrefix';


		/**
		 * Name of the table
		 * 
		 * @var String
		 */
		public $tableName;

		/**
		 * Intent
		 * 
		 * @var String
		 */
		public $intent;

		/**
		 * Site details
		 * 
		 * @var array
		 */
		public $details;

		/**
		 * Remote url
		 * 
		 * @var String
		 */
		protected $remoteUrl;

		/**
		 * Network url
		 * 
		 * @var string
		 */
		protected $networkUrl;


		/**
		 * Environment
		 * 
		 * @var ChefDeploy\Environments\Environment;
		 */
		public $env;


		/**
		 * Constructor
		 * 			
		 * @param String $tableName 
		 * @param String $intent 
		 * @param Array $siteDetails
		 *
		 * @return array
		 */
		public function __construct( $tableName, $intent, $siteDetails )
		{
			$this->tableName = $tableName;
			$this->intent = $intent;
			$this->details = $siteDetails;

			//set the remote environment
			$remoteUrl = $this->getRemoteUrl();
			$this->env = Environment::getByUrl( $remoteUrl );
		}

		/**
		 * Check if the request is valid
		 * 
		 * @return bool
		 */
		public function valid()
		{

			return true;

		}



		/**
		 * Returns the remote url
		 * 
		 * @return String
		 */
		public function getRemoteUrl()
		{
			if( 
				isset( $this->details['remote']['site_url'] ) && 
				$this->details['remote']['site_url'] != '' 
			)
				return $this->details['remote']['site_url'];

			return '';

		}
	}