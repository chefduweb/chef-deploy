<?php

	namespace ChefDeploy\Migration\Profiles;


	class SingleToProduction extends BaseProfile{


		/**
		 * Staging profile
		 * 
		 * @var string
		 */
		protected $type = 'production';

		/**
		 * Does this profile need backups?
		 * 
		 * @var bool
		 */
		protected $needsBackup = true;


		/**
		 * Returns the default old replaceables
		 * 
		 * @return Array
		 */
		protected function getReplaceOld(){

			$old = parent::getReplaceOld();

			if( $this->env->get('force_ssl') == true )
				$old[] = str_replace( 'https:', 'http:', $this->getUrl( false ) );

			return $old;
		}

		/**
		 * Returns the default new values for replacables
		 * 
		 * @return Array
		 */
		protected function getReplaceNew(){

			$new = parent::getReplaceNew();

			if( $this->env->get('force_ssl') == true )
				$new[] = str_replace(  'http:', 'https:', $this->getUrl( false ) );
			
			return $new;
		}



		/**
		 * Returns the name for this profile
		 * 
		 * @return String
		 */
		protected function getName(){
			return 'local to production';
		}



		/**
		 * Returns the action for this profile
		 * 
		 * @return string (push / pull / download)
		 */
		protected function getAction(){
			return 'push';
		}



	}