<?php

	namespace ChefDeploy\Migration\Profiles;


	class StagingToSingle extends BaseProfile{

		/**
		 * Staging profile
		 * 
		 * @var string
		 */
		protected $type = 'staging';


		/**
		 * Returns the name for this profile
		 * 
		 * @return String
		 */
		protected function getName(){
			return 'staging to local';
		}



		/**
		 * Returns the action for this profile
		 * 
		 * @return string (push / pull / download)
		 */
		protected function getAction(){
			return 'pull';
		}


/**
		 * Returns the default replaceables for this generator
		 * 
		 * @return Array
		 */
		protected function getReplaceOld(){
            
            $current = get_option( 'current_remote', false );

            $old = [
				1 => str_replace( ['https:', 'http:'], '',  $this->getUrl( false ) )
            ];
            
            if( !is_null( $this->currentMigration ) )
                $old[2] = '/uploads/sites/'.$this->currentMigration['blogId'];
            
            return $old;
		}

		/**
		 * Returns the default new values for replacables
		 * 
		 * @return Array
		 */
		protected function getReplaceNew(){
            
			$new = [
				1 => str_replace( ['https:', 'http:'], '', get_site_url() ), //-> //current-url.com
            ];

            if( !is_null( $this->currentMigration ) )
                $new[2] = '/uploads';
            
            return $new;

		}

	}