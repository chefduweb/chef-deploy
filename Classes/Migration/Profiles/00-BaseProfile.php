<?php 

	namespace ChefDeploy\Migration\Profiles;

	use ChefDeploy\Helpers\Settings;
	use ChefDeploy\Wrappers\Environment;

	/**
	 * Base profile for a WP DB Migrate Pro profile
	 */
	abstract class BaseProfile{


		/**
		 * Environment
		 * 
		 * @var ChefDeploy\Environments\BaseEnvironment;
		 */
		protected $env;


		/**
		 * Type of environment
		 * 
		 * @var String
		 */
		protected $type;


		/**
		 * Does this profile need backups?
		 * 
		 * @var bool
		 */
		protected $needsBackup = false;

        /**
         * Current migration starting
         *
         * @var Array
         */
        protected $currentMigration = null;

		/**
		 * Construct
		 * 
		 */
		public function __construct()
		{
            $this->currentMigration = $this->getCurrentMigration();
			$this->env = Environment::{$this->type}();
		}

		/**
		 * Generate this profile
		 * 
		 * @return void
		 */
		public function generate()
		{
			if( 
				is_null( $this->env->get('url') ) ||
				is_null( $this->env->get('key') )
			)
				return false;

			$args = $this->getArguments();
			$settings = get_option( 'wpmdb_settings', [] );

			if( empty( $settings ) )
				$settings = Settings::defaults();

			$settings['profiles'][] = $args;
			update_option( 'wpmdb_settings', $settings );
		
			return true;
		}


		/**
		 * Return all properties of this profile
		 * 
		 * @return Array
		 */
		public function getArguments()
		{
			return [
				'save_computer' => false,
				'gzip_file' => true,
				'replace_guids' => true,
				'exclude_spam' => true,
				'keep_active_plugins' => true,
				'create_backup' => $this->needsBackup,
				'exclude_post_types' => false,
				'exclude_transients' => true,
				'compatibility_older_mysql' => true,
				'action' => $this->getAction(),	
				'connection_info' => $this->getUrl(),
				'replace_old' => $this->getReplaceOld(),
				'replace_new' => $this->getReplaceNew(),
				'table_migrate_option' => 'migrate_only_with_prefix',
				'backup_option' => 'backup_only_with_prefix',
				'media_files' => true,
				'media_migration_option' => 'compare',
				'save_migration_profile' => true,
				'save_migration_profile_option' => false,
				'create_new_profile' => 'abel.chefduweb.nl',
				'name' => $this->getName()
			];
		}


		/**
		 * Returns the default replaceables for this generator
		 * 
		 * @return Array
		 */
		protected function getReplaceOld(){

			$profile = [
				1 => str_replace( ['https:', 'http:'], '', get_site_url() ) //-> //current-url.com
			];

			$profile = apply_filters( 'chef_deploy_default_replace_old', $profile, $this );

			return $profile;
		}

		/**
		 * Returns the default new values for replacables
		 * 
		 * @return Array
		 */
		protected function getReplaceNew(){
			
			$profile = [
				1 => str_replace( ['https:', 'http:'], '',  $this->getUrl( false ) )
			];

			$profile = apply_filters( 'chef_deploy_default_replace_new', $profile, $this );

			return $profile;
		}


		/**
		 * Returns the right url for this profile
		 *
		 * @param Bool $addKey
		 * 
		 * @return String
		 */
		public function getUrl( $addKey = true ){

			$url = $this->env->get('url');
			$key = $this->env->get('key');

			if( $addKey && !is_null( $key ) )
				$url .= "\n".$key;

			if( $this->env->get('multisite') && $addKey == false )
				$url = trailingslashit( $url ) . $this->env->get('sitepath');

			return $url;
        }
        
        /**
         * Return the current migration
         *
         * @return Array
         */
        public function getCurrentMigration()
        {
            $current = get_option( 'current_migration', false );
            if( isset( $current ) && $current !== false ){

                $time = time() - 120;//2 minutes max

                if( isset( $current['timestamp'] ) &&  $time <= $current['timestamp'] ){
                    return $current;
                }
            }

            return null;
        }


		/**
		 * Returns the name for this profile
		 * 
		 * @return String
		 */
		abstract protected function getName();



		/**
		 * Returns the action for this profile
		 * 
		 * @return string (push / pull / download)
		 */
		abstract protected function getAction();

	}