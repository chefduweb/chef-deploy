<?php

	namespace ChefDeploy\Migration\Profiles;


	class SingleToStaging extends BaseProfile{

		/**
		 * Staging profile
		 * 
		 * @var string
		 */
		protected $type = 'staging';


		/**
		 * Returns the name for this profile
		 * 
		 * @return String
		 */
		protected function getName(){
			return 'local to staging';
		}



		/**
		 * Returns the action for this profile
		 * 
		 * @return string (push / pull / download)
		 */
		protected function getAction(){
			return 'push';
		}



	}