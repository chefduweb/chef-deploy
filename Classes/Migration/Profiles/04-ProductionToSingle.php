<?php

	namespace ChefDeploy\Migration\Profiles;


	class ProductionToSingle extends BaseProfile{


		/**
		 * Staging profile
		 * 
		 * @var string
		 */
		protected $type = 'production';


		/**
		 * Does this profile need backups?
		 * 
		 * @var bool
		 */
		protected $needsBackup = true;

		/**
		 * Returns the default old replaceables
		 * 
		 * @return Array
		 */
		protected function getReplaceOld(){

			$old = parent::getReplaceOld();
            $old[] = str_replace( 'http:', 'https:', $this->getUrl( false ) );
            
            if( !is_null( $this->currentMigration ) )
                $old[] = '/uploads/sites/'.$this->currentMigration['blogId'];

			return $old;
		}

		/**
		 * Returns the default new values for replacables
		 * 
		 * @return Array
		 */
		protected function getReplaceNew(){

			$new = parent::getReplaceNew();
            $new[] = str_replace(  'https:', 'http:', $this->getUrl( false ) );
            
            if( !is_null( $this->currentMigration ) )
                $new[] = '/uploads';

			return $new;
		}



		/**
		 * Returns the name for this profile
		 * 
		 * @return String
		 */
		protected function getName(){
			return 'production to local';
		}



		/**
		 * Returns the action for this profile
		 * 
		 * @return string (push / pull / download)
		 */
		protected function getAction(){
			return 'pull';
		}



	}