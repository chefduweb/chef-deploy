<?php

	namespace ChefDeploy\Migration;

	use ChefDeploy\Helpers\Settings;
	use ChefDeploy\Wrappers\Environment;

	use ChefDeploy\Migration\Requests\FetchPrefixRequest;
	use ChefDeploy\Migration\Requests\LocalPrefixRequest;
	use ChefDeploy\Migration\Requests\RemotePrefixRequest;
	use ChefDeploy\Migration\Requests\RemoteOptionsRequest;
	use ChefDeploy\Migration\Requests\RemotePauseRequest;
    

	use ChefDeploy\Migration\Handlers\FetchPrefixHandler;
	use ChefDeploy\Migration\Handlers\LocalPrefixHandler;
	use ChefDeploy\Migration\Handlers\RemotePrefixHandler;
	use ChefDeploy\Migration\Handlers\RemoteOptionsHandler;
	use ChefDeploy\Migration\Handlers\RemotePauseHandler;
    

    class MigrationHandler{


		/**
		 * Handle a fetch-prefix request
		 * 
		 * @return Array
		 */
		public static function getRemotePrefix( $env )
		{
			//make an http-request to it's deployment url
			$requestUrl = FetchPrefixRequest::create( $env );

			if( is_null( $requestUrl ) ){
				throw new Exception( 'No valid request url found for environment '.$env->getType() );
			}

			//run that request:
            $response = wp_remote_get( $requestUrl );

			if( !is_wp_error( $response ) ){

				//return the decoded response
				return (array)json_decode( $response['body'] );

			}else{

				//throw error
				throw new Exception( 'No valid remote response given: '.$response->getMessage() );

			}
		}


		/**
		 * Change Local prefixes
		 * 
		 * @param  String $tableName   
		 * @param  String $action
		 * @param  String $stage
		 * @param  Array  $siteDetails 
		 * 
		 * @return String
		 */
		public static function localPrefix( $tableName, $action, $stage, $siteDetails = [] )
		{

			$request = new LocalPrefixRequest( $tableName, $stage, $siteDetails );

			//if the request is valid, return the new table name:
			if( $request->valid() ){

				$newName = ( new LocalPrefixHandler( $request ) )->getTableName( $action );
				$tableName = ( !is_null( $newName ) ? $newName : $tableName );

			}

			return $tableName;
		}


		/**
		 * Remote prefix handler
		 * 
		 * @param  String $tableName   
		 * @param  String $intent      
		 * @param  Array  $siteDetails 
		 * 
		 * @return String $tableName
		 */
		public static function remotePrefix( $tableName, $intent, $siteDetails = [] )
		{
			$request = new RemotePrefixRequest( $tableName, $intent, $siteDetails );

			if( $request->valid() ){

				$newName = ( new RemotePrefixHandler( $request ) )->getTableName( $intent );
				$tableName = ( !is_null( $newName ) ? $newName : $tableName );
			}

			return $tableName;
		}

		/**
		 * Set user roles, unpause if need be:
		 * 
		 * @return void
		 */
		public static function sendRemoteOptionsRequest( $env, $unpause = false )
		{
			$requestUrl = RemoteOptionsRequest::create( $env, $unpause );

			if( is_null( $requestUrl ) ){
				throw new Exception( 'No valid request url found for environment '.$request->env->getType() );
			}

			//run that request:
			$response = wp_remote_get( $requestUrl );

			return true;
		}

        /**
         * Pause the content on the remote site:
         *
         * @param $env
         * @return void
         */
        public static function sendRemotePauseRequest( $env, $pause = true ){
            
            $requestUrl = RemotePauseRequest::create( $env, $pause );

            if( is_null( $requestUrl ) ){
				throw new Exception( 'No valid request url found for environment '.$request->env->getType() );
			}

			//run that request:
			$response = wp_remote_get( $requestUrl );
        }

		/**
		 * Return the subsite media path for a pull
		 * 
		 * @param  String $original
		 * 
		 * @return String
		 */
		public static function pushMedia( $path )
		{
			if( is_multisite() ){
				$deploy = get_site_option( 'current_deployment', false );
				if( $deploy && isset( $deploy['blogId'] ) && $deploy['blogId'] != '' )
					$path = 'sites/'.$deploy['blogId'].'/'.$path;
			}

			return $path;
		}

		/**
		 * Pull media
		 * 
		 * @param  String $path
		 * 
		 * @return String
		 */
		public static function pullMedia( $path )
		{
			if( !is_multisite() ){
				return  preg_replace( "/\/sites\/([1-9]+)/", '', $path );
			}

			return $path;
		}


		/**
		 * Recreate the migration profiles for this site, after a fetch
		 * 
		 * @return void
		 */
		public static function resetMigrationProfiles()
		{

			$args = [
    			'allow_pull' => true,
    			'allow_push' => true,
			];
			
			delete_option( 'profiles-generated' );
			$default = Settings::defaults();
			$args = wp_parse_args( $args, $default );
			
			if( is_multisite() ){
				update_site_option( 'wpmdb_settings', $args );
			}else{
				update_option( 'wpmdb_settings', $args );
			}	
			
		}
	}