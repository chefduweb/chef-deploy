<?php

	namespace ChefDeploy\Migration\Handlers;

	/**
	 * Runs on Remote
	 */
	class BlogIdHandler extends BaseHandler{


		/**
		 * User making the request
		 * 
		 * @var void
		 */
		protected $user;



		/**
		 * Process a valid request, on the REMOTE env
		 * 
		 * @return void
		 */
		public function process()
		{
            global $wpdb;

            $path = $this->request->get('sitepath');
            if( substr( $path, 0, 1 ) != '/' ){
                $path = '/'.$path;
            }

            if( substr( $path, -1 ) != '/' ){
                $path = $path .'/';
            }

            $result = $wpdb->get_var( $wpdb->prepare( "SELECT blog_id FROM {$wpdb->prefix}blogs WHERE `path`=%s", $path ) );
            if( !is_null( $result ) ){
                echo $result;
            }else{
                echo 1;
            }

            die();
		}

	}