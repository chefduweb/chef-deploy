<?php

	namespace ChefDeploy\Migration\Handlers;

	use Exception;
	use ChefDeploy\Helpers\Table;
	use ChefDeploy\Migration\MigrationHandler;
	use ChefDeploy\Migration\Requests\FetchPrefixRequest;

	class LocalPrefixHandler extends BaseHandler{



		/**
		 * Returns a given table name
		 * 
		 */
		public function getTableName( $intent = 'push' )
		{

			global $wpdb;

			if( $intent == 'push' ){
				
				$remote = $this->getRemoteSite();

				//if the remote Site's response isn't valid:
				if( !$this->validate( $remote ) )
					return $this->request->tableName;

				$oldPrefix = $wpdb->base_prefix; //veenstra_
				$newPrefix = $remote['prefix']; //abel_23_
			
			}else{
					
				$deploy = get_site_option( 'current_deployment' );
				$oldPrefix = $deploy['prefix']; //abel_23
				$newPrefix = $this->request->details['local']['prefix']; // veenstra

			}	


			return Table::switchPrefix( 
				$oldPrefix,
				$newPrefix,
				$this->request->tableName
			);
		}


		/**
		 * Validate the remote response
		 * 
		 * @return boolean
		 */
		public function validate( $remoteSite )
		{

			if( !is_array( $remoteSite ) || empty( $remoteSite ) )
				return false;
			
			//if we're synching the main blog, or we're backing up, return the regular table name
			if( $remoteSite['blogId'] == 1 )
				return false;

			//if there isn't a new prefix, return the old table name:
			if( !isset( $remoteSite['prefix'] ) )
				return false;


			return true;
		}


		/**
		 * Returns the information of the remote site
		 * 
		 * @return Array
		 */
		public function getRemoteSite()
		{			
			try{

				$remote = MigrationHandler::getRemotePrefix( $this->request->env );
				return $remote;
			
			} catch( Exception $e ){

				echo $e->getMessage();
				die();

			}
		}
	}