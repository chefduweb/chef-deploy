<?php

	namespace ChefDeploy\Migration\Handlers;

	/**
	 * Runs on Remote
	 */
	class AddAdminsHandler extends BaseHandler{


		/**
		 * User making the request
		 * 
		 * @var void
		 */
		protected $user;



		/**
		 * Process a valid request, on the REMOTE env
		 * 
		 * @return void
		 */
		public function process()
		{
            global $wpdb;

            $path = $this->request->get('sitepath');
            if( substr( $path, 0, 1 ) != '/' ){
                $path = '/'.$path;
            }

            if( substr( $path, -1 ) != '/' ){
                $path = $path .'/';
            }

            $blogId = $wpdb->get_var( $wpdb->prepare( "SELECT blog_id FROM {$wpdb->prefix}blogs WHERE `path`=%s", $path ) );

            if( !is_null( $blogId ) ){

                //add super admins to blog
                $admins = get_super_admins();

                foreach( $admins as $admin ){
                    $_user = get_user_by( 'login', $admin );
                    add_user_to_blog( $blogId, $_user->ID, "administrator" );
                }
            }
            die();
		}

	}