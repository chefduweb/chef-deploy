<?php

	namespace ChefDeploy\Migration\Handlers;

	abstract class BaseHandler{


		/**
		 * Request
		 * 
		 * @var ChefDeploy\Migration\Requests\BaseRequest
		 */
		protected $request;

		/**
		 * Constructor
		 * 
		 * @param ChefDeploy\Migration\Requests\BaseRequest $request
		 *
		 * @return void
		 */
		public function __construct( $request )
		{
			$this->request = $request;
		}

	}