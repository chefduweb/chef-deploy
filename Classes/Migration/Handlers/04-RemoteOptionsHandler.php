<?php

	namespace ChefDeploy\Migration\Handlers;

	use ChefDeploy\Helpers\Settings;

	class RemoteOptionsHandler extends BaseHandler{

		/**
		 * Process a User Roles request
		 * 
		 * @return void
		 */
		public function process()
		{

			global $wpdb;
			$deployment = get_site_option( 'current_deployment' );

			$blogId = $deployment['blogId'];
			//echo $blogId;
			$old = $deployment['oldPrefix'];
			$new = $deployment['prefix'];

			//set the user roles
			switch_to_blog( $blogId );
            $default = Settings::defaultRoles();
            
            do_action( 'chef_deploy_remote_options', $blogId );

			update_option( $new.'user_roles', $default );
			update_option( 'user_roles', $default );

			//set the allowed theme:
			$theme = $this->request->get('name');
			$allowed = [ $theme => true ];
            update_option( 'allowedthemes', $allowed ); 
            
            $unpause = $this->request->get( 'unpause' );
            if( !is_null( $unpause ) && $unpause != false && $unpause != 'none' ){
                update_option( 'site_content_paused', false );
            }

			//delete the current deployment:
			//delete_site_option( 'current_deployment' );
			//
			return true;

		}

			
	

	}