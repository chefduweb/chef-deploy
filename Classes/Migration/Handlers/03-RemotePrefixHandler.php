<?php

	namespace ChefDeploy\Migration\Handlers;

	use ChefDeploy\Helpers\Table;
	use ChefDeploy\Migration\MigrationHandler;

	class RemotePrefixHandler extends BaseHandler{


		/**
		 * Return the new table name
		 * 
		 * @return String
		 */
		public function getTableName( $intent )
		{	
			global $wpdb;
			

			if( $intent == 'push' ){
				
				$deployment = get_site_option( 'current_deployment' );
				$oldPrefix = $this->request->details['local']['prefix'];
				$newPrefix = $deployment['prefix'];

			}else{

				$remote = MigrationHandler::getRemotePrefix( $this->request->env );
				$oldPrefix = $remote['prefix'];
				$newPrefix = $this->request->details['local']['prefix'];
			}
			

			return Table::switchPrefix( 
				$oldPrefix, 
				$newPrefix, 
				$this->request->tableName
			);

		}

	}