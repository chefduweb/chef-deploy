<?php

	namespace ChefDeploy\Migration\Handlers;

	/**
	 * Runs on Remote
	 */
	class FetchPrefixHandler extends BaseHandler{


		/**
		 * User making the request
		 * 
		 * @var void
		 */
		protected $user;



		/**
		 * Request 
		 * 
		 * @param DeployRequest $request
		 *
		 * @return void
		 */
		public function __construct( $request )
		{
			parent::__construct( $request );
			$this->user = $request->userExists( $request->get( 'user' ) );
		}



		/**
		 * Process a valid request, on the REMOTE env
		 * 
		 * @return void
		 */
		public function process()
		{

			global $wpdb;

			try{

				$payload = [
					'blogId' => 'main',
					'prefix' => $wpdb->prefix,
				];
					
				//deal with the multisite problem:
				if( is_multisite() ){
					$blogId = $this->getOrCreateBlogId();

					$payload = [
						'blogId' => $blogId,
						'prefix' => $this->getMultisitePrefix( $blogId )
					];
                }
                
				//temp save the current deployment
				update_site_option( 'current_deployment', array_merge(
					$payload, 
					[ 'oldPrefix' => $this->request->get('prefix')]
				));

			}catch( Exception $e ){

				Logger::error( $e->getMessage() );
				$payload = [ 'error' => true, 'message' => $e->getMessage() ];
			}

			echo json_encode( $payload );
			die();
		}


		/**
		 * Return a blog ID, either way
		 * 
		 * @return String
		 */
		public function getOrCreateBlogId()
		{

			//find the blogId
			$blogId = get_blog_id_from_url( 
				$this->request->get( 'url' ), 
				$this->request->get( 'sitepath' )
			);

			//no blog ID could be found, let's create one
			if( $blogId == 0 ){
				extract( $this->getBlogArgs() );
				$blogId = wpmu_create_blog( $domain, $sitepath, $title, $user_id, $meta );
                
                //add super admins to blog
                $admins = get_super_admins();
                foreach( $admins as $admin ){
                    $_user = get_user_by( 'login', $admin );
                    add_user_to_blog( $blogId, $_user->ID, "administrator" );
                }
            }


			//creating failed:
			if( is_wp_error( $blogId ) ){
				throw new Exception( $blogId->get_error_message() );
				return false;
			}

			return $blogId;
		
		}

		/**
		 * Return the arguments for our new blog
		 * 
		 * @return Array
		 */
		public function getBlogArgs()
		{
			$sitepath = $this->request->get( 'sitepath' );
			return [
				'domain' => $this->request->get( 'url' ),
				'sitepath' => $sitepath,
				'title' => ucwords( str_replace( '-', ' ', $sitepath ) ),
				'user_id' => $this->user->ID,
				'meta' => [ 'public' => 1 ]
			];
		}


		/**
		 * Return the right prefix for this blog
		 * 
		 * @return String
		 */
		public function getMultisitePrefix( $blogId )
		{
			global $wpdb;
			$prefix = $wpdb->base_prefix;
			
			return $prefix.$blogId.'_';
		}
	}