<?php

	namespace ChefDeploy\Migration\Handlers;

	use ChefDeploy\Helpers\Settings;

	class RemotePauseHandler extends BaseHandler{

		/**
		 * Process a User Roles request
		 * 
		 * @return void
		 */
		public function process()
		{
            $pause = $this->request->get( 'pause' );

            if( !is_null( $pause ) && $pause != false ){
                
                if( $pause == 'pause' ){
                    update_option( 'site_content_paused', true );
                }else if( $pause == 'unpause' ){
                    update_option( 'site_content_paused', false );
                }
            }

			return true;

		}

	}