<?php

    namespace ChefDeploy\Wrappers;
    
    class Environment extends Wrapper {
    
        /**
         * Return the igniter service key responsible for the Environment class.
         * The key must be the same as the one used in the assigned
         * igniter service.
         *
         * @return string
         */
        protected static function getFacadeAccessor(){
            return 'environment';
        }
    
    }
