<?php

	namespace ChefDeploy\Cli;

	use WP_CLI;
	use WP_CLI_Command;
    use ChefDeploy\Helpers\Settings;
	use ChefDeploy\Migrations\Generator;
	use ChefDeploy\Wrappers\Environment;
	use ChefDeploy\Migration\MigrationHandler;


	class DeployCommands extends WP_CLI_Command{
	
		/**
		 * Deploy to staging
		 * 
		 * @param  array $args
		 * @param  array $assoc_args
		 * 
		 * @return WP_CLI::success message
		 */
    	function staging( $args, $assoc_args )
    	{

    		do_action( 'chef_deploy_before_deploy_staging', $args, $assoc_args );

			WP_CLI::log( "Starting migration..." );

			MigrationHandler::resetMigrationProfiles();
			Generator::getInstance();
    		
    		//migrate profile 1 = local to staging    	
    		exec( 'wp migratedb profile 1' );

    		WP_CLI::log( "Updating remote site..." );

    		//reset user-roles on remote:
            $env = Environment::staging();
            $unpause = isset( $assoc_args['unpause'] );
            
            if( $unpause ){
                WP_CLI::log( 'Unpausing content on the remote site' );
            }else{
                WP_CLI::log( 'Not unpausing content on the remote site; it remains locked down.' );
            }

    		MigrationHandler::sendRemoteOptionsRequest( $env, $unpause );

    		// Print a success message
			WP_CLI::success( "Succesfully deployed to staging" );
			
			do_action( 'chef_deploy_after_deploy_staging', $args, $assoc_args );

    	}
		

    	/**
    	 * Deploy to production
    	 * 
    	 * @param  Arry $args       
    	 * @param  Arry $assoc_args 
    	 * 
    	 * @return WP_CLI::success message
    	 */
		function production( $args, $assoc_args )
		{
			do_action( 'chef_deploy_before_deploy_production', $args, $assoc_args );

			WP_CLI::log( "Starting migration..." );

			MigrationHandler::resetMigrationProfiles();
			Generator::getInstance();

			exec( 'wp migratedb profile 2' );

    		WP_CLI::log( "Updating remote site..." );

			//reset user-roles on remote:
            $env = Environment::production();
            $unpause = isset( $assoc_args['unpause'] );
            
            if( $unpause ){
                WP_CLI::log( 'Unpausing content on the remote site' );
            }else{
                WP_CLI::log( 'Not unpausing content on the remote site; it remains locked down.' );
            }
            
    		MigrationHandler::sendRemoteOptionsRequest( $env, $unpause );

			WP_CLI::success( "Succesfully deployed to production" );

			do_action( 'chef_deploy_after_deploy_production', $args, $assoc_args );
        }
	

		/**
		 * Create the profiles
    	 * 
    	 * @param  Arry $args       
    	 * @param  Arry $assoc_args 
    	 * 
		 * @return WP_CLI::success message
		 */
		public function createProfiles( $args, $assoc_args )
		{
			WP_CLI::log( "Creating deployment profiles..." );	

			//generate migration profiles, to be sure:
			MigrationHandler::resetMigrationProfiles();
			Generator::getInstance();

			WP_CLI::success( "Succesfully generated profiles" );
		}
	
	}


	WP_CLI::add_command( 'deploy', 'ChefDeploy\Cli\DeployCommands' );

