<?php

	namespace ChefDeploy\Cli;

	use WP_CLI;
    use WP_CLI_Command;
	use ChefDeploy\Helpers\UserRoles;
    use ChefDeploy\Migration\Tables;
	use ChefDeploy\Migrations\Generator;
	use ChefDeploy\Wrappers\Environment;
	use ChefDeploy\Migration\MigrationHandler;

	class FetchCommands extends WP_CLI_Command{
	
		/**
		 * Deploy to staging
		 * 
		 * @param  array $args
		 * @param  array $assoc_args
		 * 
		 * @return WP_CLI::success message
		 */
    	public function staging( $args, $assoc_args )
    	{

    		do_action( 'chef_deploy_before_fetch_staging', $args, $assoc_args );

    		$env = Environment::staging();
            
            WP_CLI::log( "Resetting remote site information..." );
            $this->resetRemote( $env );

			WP_CLI::log( "Resetting local profiles..." );
            $this->resetProfiles();
        
            
            WP_CLI::log( "Starting migration..." );
            exec( "wp migratedb profile 3" );

    		//reset migration profiles after database change
    		$this->resetProfiles();

    		//reset user-roles on local:
    		UserRoles::reset();

            //pause the remote site:
            if( isset( $assoc_args['pause'] ) ){
                WP_CLI::log( 'Pausing content on the staging site' );
                MigrationHandler::sendRemotePauseRequest( $env, true );
            }

    		// Print a success message
			WP_CLI::success( "Succesfully fetched from staging" );
		
	        do_action( 'chef_deploy_after_fetch_staging', $args, $assoc_args );

    	}
		

    	/**
    	 * Deploy to production
    	 * 
    	 * @param  Arry $args       
    	 * @param  Arry $assoc_args 
    	 * 
    	 * @return WP_CLI::success message
    	 */
		public function production( $args, $assoc_args )
		{

			do_action( 'chef_deploy_before_fetch_production', $args, $assoc_args );

			$env = Environment::production();

            WP_CLI::log( "Resetting remote site information..." );
            $this->resetRemote( $env );

            WP_CLI::log( "Resetting local profiles..." );
            $this->resetProfiles();
                        
            
            WP_CLI::log( "Starting migration..." );
            exec( "wp migratedb profile 4" );            

    		//reset migration profiles after database change
    		$this->resetProfiles();

    		//reset user-roles on local:
            UserRoles::reset();

            //pause the remote site:
            if( isset( $assoc_args['pause'] ) ){
                WP_CLI::log( 'Pausing content on the production site' );
                MigrationHandler::sendRemotePauseRequest( $env, true );
            }
            			
			WP_CLI::success( "Succesfully deployed to production" );

			do_action( 'chef_deploy_after_fetch_production', $args, $assoc_args );

		}

        /**
         * Clean up tables
         *
         * @return void
         */
        public function cleanup( $args, $assoc_args )
        {   
            $name = $args[0];
            $toplevel = $args[1];

            WP_CLI::log( "Cleaning up tables..." );

            $tables = new Tables();
            $tables->cleanUp();

            //replace urls:
            wp_cache_flush();
            $url = get_option( 'siteurl' );
            $newUrl = 'http://'. $name . $toplevel;
            
            update_option( 'blogname', $name );
            exec( "wp search-replace $url $newUrl" );
            exec( "wp theme activate $name" );

            //reset migration profiles after database change
            $this->resetProfiles();

            //reset user-roles on local:
            UserRoles::reset();

            WP_CLI::success( "Tables cleaned up." );
        }

		/**
		 * Reset profiles
		 *
		 * @return Boolean
		 */
		public function resetProfiles(){

			//generate migration profiles, to be sure:
			MigrationHandler::resetMigrationProfiles();
			Generator::getInstance();
		
        }
        
        /**
         * Create a request to the remote first, setting up the current site:
         *
         * @param Environment $env
         * @return void
         */
        public function resetRemote( $env )
        {
            $remote = MigrationHandler::getRemotePrefix( $env );
            $remote['timestamp'] = time();

            //update the current migration
            update_option( 'current_migration', $remote );
        }
	
	
	}


	WP_CLI::add_command( 'fetch', 'ChefDeploy\Cli\FetchCommands' );

