<?php


	namespace ChefDeploy\Environment;

	use Dotenv\Dotenv;
	use Cuisine\Utilities\Fluent;

	/**
	 * Base environment
	 */
	class BaseEnvironment extends Fluent{


		/**
		 * Array of attributes
		 * 
		 * @var Array
		 */
		protected $attributes;



		/**
		 * Type of environment
		 * 
		 * @var String
		 */
		protected $type;


		/**
		 * Constructor
		 * 
		 * @param array $props
		 */
		public function __construct( $props = [] )
		{

			$properties = [];
			if( !empty( $props ) ){
				foreach( $props as $key => $prop ){
					$properties[ strtoupper( $key ) ] = $prop;
				}
			}

			$this->attributes = $properties + $this->getDotEnv();

		}



		/****************************************/
		/**           Getters & Setters         */
		/****************************************/

		/**
		 * Get a specific property
		 * 
		 * @param  String  $name    
		 * @param  mixed $default 
		 * 
		 * @return Mixed
		 */
		public function get( $name, $default = null )
		{
			return parent::get( strtoupper( $name ), $default );
		}



		/**
		 * Set the dotenv variables
		 *
		 * @return Array
		 */
		public function getDotEnv()
		{
			//set the correct path
			$path = trailingslashit( \ChefDeploy\PluginIgniter::getPluginPath() );
			$path .= 'Configs';

			if( file_exists( trailingslashit( $path ). $this->type.'.env' ) ){

				//load the dotenv
				( new Dotenv( $path, $this->type.'.env' ) )->overload();

				//save the values in a temporary variable
				$env = $_ENV;

				//prevent leaking in super globals, 
				//we only need the values in this object
				foreach( $env as $key => $value ){
					unset( $_ENV[ $key ], $_SERVER[ $key ] );
				}

				return $env;

			}

			return [];
		}


		/**
		 * Returns the type of environment
		 * 
		 * @return String
		 */
		public function getType()
		{
			return $this->type;
		}

	}