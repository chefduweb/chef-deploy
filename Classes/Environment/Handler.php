<?php

	namespace ChefDeploy\Environment;


	class Handler{


		/**
		 * Magic method that returns an environment
		 * 		 
		 * @param string $name Name of the method
     	 * @param  array $attr
     	 * 
		 * @return ChefDeploy\Environments\LocalEnvironment
		 */
		public function __call( $name, $attr )
		{
			$envs = $this->getEnvironments();
			
			if( !isset( $envs[ $name ] ) )
				return false;

			//return a new instance of the class, with the props
			$class = $envs[ $name ]['class'];
			return new $class( $envs[ $name ]['props'] );
		}

		/**
		 * Returns the environment based on it's url
		 *
		 * @param  String $url
		 * 
		 * @return ChefDeploy\Environments\Environment
		 */
		public function getByUrl( $remoteUrl )
		{
			$remoteUrl = $this->sanitizeUrl( $remoteUrl );
			$environments = $this->getEnvironments();

			foreach( $environments as $type => $env ){

				$class = $env['class'];
				$obj = new $class( $env['props'] );
                
				//check if this object exists:
				if( is_null( $obj->get( 'url' ) ) )
					continue;

				$envUrl = $this->sanitizeUrl( $obj->get('url') );

				if( $envUrl == $remoteUrl ){
					return $obj;
					break;
				}
			}

			return null;
		}


		/**
		 * Sanitize an url for comparison
		 *
		 * @param String $url
		 * 
		 * @return String
		 */
		public function sanitizeUrl( $url )
		{
			//remove trailing slash:
			$url = rtrim( $url,"/" );

			//remove protocol:
			$url = str_replace(['http:', 'https:'], '', $url );

			//and make sure it's all lowercase:
			$url = strtolower( $url );

			return $url;
		}


		/**
		 * Returns a full list of environments
		 * 
		 * @return Array
		 */
		public function getEnvironments()
		{
			$envs = [
				'local' => [
					'class' => '\\ChefDeploy\\Environment\\Local',
					'props' => [] 
				],

				'staging' => [
					'class' => '\\ChefDeploy\\Environment\\Staging',
					'props' => []
				],

				'production' => [
					'class' => '\\ChefDeploy\\Environment\\Production',
					'props' => []
				]
			];


			$envs = apply_filters( 'chef_deploy_environments', $envs );
			return $envs;	
		}
	}