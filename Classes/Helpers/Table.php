<?php

	namespace ChefDeploy\Helpers;


	class Table{

		/**
		 * Check if a table is globally scoped
		 *
		 * @param  String $tableName
		 * 
		 * @return boolean
		 */
		public static function isGlobal( $table )
		{
			global $wpdb;
			$globalTables = array_values( $wpdb->tables( 'global' ) );
			return in_array( $table, $globalTables );

		}


		/**
		 * Switch prefix
		 * 
		 * @param  String $old
		 * @param  String $new  
		 * @param  String $table
		 * 
		 * @return String
		 */
		public static function switchPrefix( $old, $new, $table )
		{
			if ( stripos( $table, $old ) === 0 )
				return substr_replace( $table, $new, 0, strlen( $old ) );

			return null;
		}
	}