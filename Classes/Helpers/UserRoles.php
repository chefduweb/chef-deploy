<?php
	
	namespace ChefDeploy\Helpers;


	class UserRoles{

		/**
		 * Reset the user-roles to the default
		 * 
		 * @return void
		 */
		public static function reset()
		{
			global $wpdb;
			$prefix = $wpdb->prefix;
			$default = Settings::defaultRoles();

			update_option( $prefix.'user_roles', $default );
			update_option( 'user_roles', $default );
		}


	}